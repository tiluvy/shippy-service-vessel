module bitbucket.org/tiluvy/shippy-service-vessel

go 1.12

require (
	github.com/golang/protobuf v1.3.2
	github.com/micro/go-micro v1.11.1
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	golang.org/x/net v0.0.0-20191003171128-d98b1b443823
)
