package main

import (
	pb "bitbucket.org/tiluvy/shippy-service-vessel/proto/vessel"
	"context"
	"errors"
	"github.com/micro/go-micro"
	"fmt"
	"log"
)

type Repository interface {
	FindAvailable(*pb.Specification) (*pb.Vessel, error)
}

type VesselRepository struct {
	vessels []*pb.Vessel
}

func (repo *VesselRepository) FindAvailable(spec *pb.Specification) (*pb.Vessel, error) {
	for _, vessel := range repo.vessels {
		log.Print(vessel)
		if vessel.Capacity >= spec.Capacity && vessel.MaxWeight >= spec.MaxWeight {
			return vessel, nil
		}
	}
	return nil, errors.New("no vessel found by that spec")
}

type service struct {
	repo Repository
}

func (s *service) FindAvailable (ctx context.Context, req *pb.Specification, res *pb.Response) error {
	vessel, err := s.repo.FindAvailable(req)
	if err != nil {
		return err
	}
	res.Vessel = vessel
	return nil
}

func main() {

	vesselService := service{&VesselRepository{
		[]*pb.Vessel {
			{Id:"vessel001", Name: "", MaxWeight:200000, Capacity:500},
		},
	}}
	srv := micro.NewService(
		micro.Name("shippy.service.vessel"),
		micro.Version("latest"),
	)
	srv.Init()

	pb.RegisterVesselServiceHandler(srv.Server(), &vesselService)

	if err := srv.Run(); err != nil {
		fmt.Print(err)
	}
}